package main.java.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AMARIS_TWEET")
public class AmarisTweet {
	@Id
	public Long id;
	public String data;
	
	public AmarisTweet()
	{
		
	}
	
	public AmarisTweet(Long id, String data)
	{
		this.setId(id);
		this.setData(data);
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}

package main.java.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
	@Id
	String id;
	
	
	String name;
	
	String password;
	
	String roles;
	
	public void setId(String id)
	{
		this.id = id;
	}
	
	public String getId()
	{
		return id;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}
	
	public String getPassword()
	{
		return password;
	}
	
	public void setRoles(String roles)
	{
		this.roles = roles;
	}
	
	public String getRoles()
	{
		return roles;
	}
	
	@Override
	public String toString()
	{
		return "id: "+id+", name"+name+", passowrd: "+password+", roles: "+this.roles;
	}
}

package main.java.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import main.java.services.UserAuthenticationProvider;

@Configuration
@EnableWebSecurity
public class SecurityConfig
   extends WebSecurityConfigurerAdapter {

  @Autowired
  UserAuthenticationProvider userAuthenticationProvider;
  
  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	  
    /*auth
      .inMemoryAuthentication()
        .withUser("user").password("password").roles("USER")
        .and()
      	.withUser("admin").password("password").roles("USER", "ADMIN");*/
    
   auth.authenticationProvider(userAuthenticationProvider);

  }
 
  
  @Override
  protected void configure(HttpSecurity http) throws Exception {
	  //
	 // http.authorizeRequests().anyRequest().authenticated().and().formLogin().and().httpBasic();
	  //
	  http
	  .authorizeRequests()
	    .antMatchers("/console/**").permitAll()
	  	//.antMatchers("/**").permitAll()
	  	//.antMatchers("/loginpage/**").permitAll() 
	    .antMatchers("/adminpage/**").hasAnyAuthority( "admin")
	  	.anyRequest().hasAnyAuthority("user", "admin")
	  	//.anyRequest().authenticated()
	  .and()
	  	.formLogin();
	  
	  // to enable h2
	  http.csrf().disable();
	
	  // to enabl2 h2
	  http.headers().frameOptions().disable();
	  	//.loginPage("myloginpage");
	 //UserDetails temp = na
 }
  

  public BCryptPasswordEncoder passwordEncoder() {
      return new BCryptPasswordEncoder();
  }
  
  @Bean
  public MyPasswordEncoder myPasswordEncoder() {
      return new MyPasswordEncoder();
  }
  
  
  public class MyPasswordEncoder implements PasswordEncoder {

		/**
		 * Encode the raw password. Generally, a good encoding algorithm applies a SHA-1 or
		 * greater hash combined with an 8-byte or greater randomly generated salt.
		 */
	  
		public String encode(CharSequence rawPassword)
		{
			return rawPassword.toString();
		}

		/**
		 * Verify the encoded password obtained from storage matches the submitted raw
		 * password after it too is encoded. Returns true if the passwords match, false if
		 * they do not. The stored password itself is never decoded.
		 *
		 * @param rawPassword the raw password to encode and match
		 * @param encodedPassword the encoded password from storage to compare with
		 * @return true if the raw password, after encoding, matches the encoded password from
		 * storage
		 */
		public boolean matches(CharSequence rawPassword, String encodedPassword)
		{
			return this.encode(rawPassword).equals(encodedPassword);
		}

  	}
  
  

}
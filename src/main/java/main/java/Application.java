package main.java;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.ImportResource;
import org.springframework.scheduling.annotation.EnableScheduling;

import main.java.controller.MyRestController;
import main.java.data.UserRepository;
import main.java.model.User;
import main.java.services.TwitterService;

@SpringBootApplication
@EnableCaching
@EnableCircuitBreaker
@EnableScheduling
@ImportResource("classpath:users.xml")  
public class Application  extends SpringBootServletInitializer  implements CommandLineRunner {
	@Autowired 
	MyRestController myRest ; 
	@Autowired 
	UserRepository userRep ; 
	@Autowired
	private List<User> defaultUsers;
	@Autowired
	private TwitterService twitterService;
	
	public static void main(String[] args)
	{
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Started, default users: "+defaultUsers.size());
		//
		for(User user: defaultUsers)
		{
			userRep.save(user);
			//
			System.out.println("saved: "+user);
		}
		//
		System.out.println(twitterService.GetTimeLine("amaris"));
	}
	

}

package main.java.data;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import main.java.model.User;


@Repository
public interface UserRepository extends CrudRepository<User, String>  {
	
	//
	List<User> findByName(String name);
	
	@Cacheable("UserRepository.findAllCached")
	default public Iterable<User> findAllCached()
	{
		try {
			System.out.println("before sleep");
			Thread.sleep(5000);
			System.out.println("after sleep");
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.findAll();
	}
}

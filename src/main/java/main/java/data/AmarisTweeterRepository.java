package main.java.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import main.java.model.AmarisTweet;

@Repository
public interface AmarisTweeterRepository extends CrudRepository<AmarisTweet, Long>  {

}

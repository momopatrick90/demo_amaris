package main.java.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Role;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import main.java.data.UserRepository;
import main.java.model.User;

import java.util.logging.Level;
import java.util.logging.Logger;


@Service
public class UserAuthenticationProvider implements AuthenticationProvider {
	//
	private final static Logger LOGGER = Logger.getLogger(UserAuthenticationProvider.class.getName());
	
	//
	@Autowired
	UserRepository userRepository = null;
	
	@Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
  
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();

        //
        List<User> users = userRepository.findByName(name);
        //
        boolean passwordExists = users != null &&  
        			users.stream().filter(u -> u.getPassword().equals(password) ).count() != 0;
        
        if (passwordExists) {
            return new UsernamePasswordAuthenticationToken(
              name, password, AuthorityUtils.commaSeparatedStringToAuthorityList(users.get(0).getRoles()));
        } else {
        	//
        	LOGGER.log(Level.INFO, "*************user "+name+"not found");
            return null;
        }
    }



	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(
		          UsernamePasswordAuthenticationToken.class);
	}

}

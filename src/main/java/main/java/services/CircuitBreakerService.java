package main.java.services;

import org.springframework.stereotype.Service;

import com.netflix.hystrix.HystrixCommand.Setter;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import java.util.Random;

@Service
public class CircuitBreakerService {
	Random rand = new Random();

	@HystrixCommand(fallbackMethod = "callFallBack")
	public String call()
	{

		/*Setter.withGroupKey(null).andCommandPropertiesDefaults(null).
		//
		HystrixCommandProperties.Setter()
		.withCircuitBreakerRequestVolumeThreshold(10)
		.withCircuitBreakerSleepWindowInMilliseconds(5000)
		.withCircuitBreakerErrorThresholdPercentage(30)*/
		
		/*//
		int volumeThreshHold = 10;
		int sleepMillisecs = 5000;
		int errorThresholdPercentage = 50;
		//
		HystrixCommandProperties.Setter()
			.withCircuitBreakerRequestVolumeThreshold(volumeThreshHold)
			.withCircuitBreakerSleepWindowInMilliseconds(sleepMillisecs)
			.withCircuitBreakerErrorThresholdPercentage(errorThresholdPercentage);
		//circuitBreaker.requestVolumeThreshold  = 1;
		//circuitBreaker.sleepWindowInMilliseconds = 5000;*/
		//
		double temp = rand.nextDouble();
		
		/*HystrixCommandProperties.Setter()
		   .withCircuitBreakerRequestVolumeThreshold(10)*/
		
		if(temp < 0.6)
		{
			throw new RuntimeException("call to service failed");
		}
		return "call to service by circuit break succeeded: rand: "+temp;
	}
	
	public String callFallBack()
	{
		System.out.print("falling back");
		return "fall back";
	}
}

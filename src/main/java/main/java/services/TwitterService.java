package main.java.services;

import java.util.LinkedList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import main.java.data.AmarisTweeterRepository;
import main.java.model.AmarisTweet;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

@Service
public class TwitterService {
	  @Value("${twitter.key}")
	  static String ConsumerKey = "qnI3LFzzMpgevTcfPzQ70qqUx";
	  @Value("${twitter.secret}")
	  static String ConsumerSecret = "husd2NspU1qoIL6GVxdD5iJGTTUWJQNPJFy0lopkoAMixMCJBT";
	  @Autowired
	  AmarisTweeterRepository repository;
	  
	  public JsonNode GetTimeLine(String username) throws Exception {
		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(
              ConsumerKey,
              ConsumerSecret);
		  //
		  String url = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name="+username;
	      //
	      HttpGet request = new HttpGet(url);
	      consumer.sign(request);
	
	      HttpClient client = new DefaultHttpClient();
	      HttpResponse response = client.execute(request);
	
	      int statusCode = response.getStatusLine().getStatusCode();
	      System.out.println(statusCode + ":" + response.getStatusLine().getReasonPhrase());
	      
	      //
	      String responseString = EntityUtils.toString(response.getEntity());
	      //System.out.println();
	      
	      if(statusCode != 200)
	      {
	    	  throw new Exception("Error getting status");
	      }
	      
	      return new ObjectMapper().readTree(responseString);
	}

	@Scheduled(fixedRate = 1000*60)
	public void saveAmarisTweets() {
		//
		System.out.println("getting tweets tweet");
		
		try {
			//
			JsonNode node = this.GetTimeLine("amaris");
			//
			System.out.println("got tweets tweet"+node.toString());
			//
			for (final JsonNode tweetJsonNode : node) {
				//
				Long id =  tweetJsonNode.get("id").asLong();
				//
				AmarisTweet tweet = new AmarisTweet(id, tweetJsonNode.toString());
				//
				//System.out.println("Saving: "+id+" "+repository.findById(tweet.getId()));
				//
				if(!repository.findById(tweet.getId()).isPresent())
				{
					//repository.save(tweet);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}

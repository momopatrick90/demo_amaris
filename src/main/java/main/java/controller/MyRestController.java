package main.java.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import main.java.data.AmarisTweeterRepository;
import main.java.data.UserRepository;
import main.java.model.AmarisTweet;
import main.java.model.User;
import main.java.services.CircuitBreakerService;

@RestController
public class MyRestController {
	@Autowired
	RequestMappingHandlerMapping handlerMapping;
	
	@Autowired
	UserRepository userRep;
	
	@Autowired
	CircuitBreakerService circuitBreakerService;
	
	@Autowired
	AmarisTweeterRepository tweeterRepository;
	
	@GetMapping("/")
	public String root()
	{
		//
		String mappings = "";
		
		//
		for(RequestMappingInfo info: handlerMapping.getHandlerMethods().keySet())
		{
			mappings += info.toString() + "<br />";
		}
		
		
		return mappings;
		
	}
	
	
	
	@GetMapping("/index.html")
	public String index()
	{
		return root();
	}
	

	@RequestMapping(value = "/loginpage", method = RequestMethod.GET)
    public String login_page() {
        return "<body>login page</body>";
	}
	
	@RequestMapping(value = "/adminpage", method = RequestMethod.GET)
    public Iterable<User> adminpage()
    {
        return users_cached();
	}

	@RequestMapping(value = "/users/list/cached", method = RequestMethod.GET)
    public Iterable<User>  users_cached()
    {
		//
		
		//
		Iterable<User> users = userRep.findAllCached();
		//
		return users;
	}
	
	
	@RequestMapping(value = "/circuitbreak", method = RequestMethod.GET)
    public String circuitbreak()
    {
		return circuitBreakerService.call();
	}
	
	
	@RequestMapping(value = "/amaristweets", method = RequestMethod.GET)
    public Iterable<AmarisTweet> amaristweets()
    {
		return tweeterRepository.findAll();
	}

	

}

package demo_amaris;

import org.apache.catalina.core.ApplicationContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.*;
import main.java.model.User;


@RunWith(SpringRunner.class)
@org.springframework.test.context.ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class AppTest {
	
	 @Configuration
	 static class TestConf {
		 // this bean will be injected into the OrderServiceTest class
		 @Bean
	     public User orderService() {
			 //
			 User user = new User();
			 //
			 user.setId("someId");
	         // set properties, etc.
	         return user;
	     }
	 }
	
	@Autowired
	public User user;

	
	@Test
	public void TestBeansDefined()
	{
		assertNotNull(user);
	}
}
